from django.db import models
from django_resized import ResizedImageField
from django.contrib.auth.models import User
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = ResizedImageField(size=[200, 200], quality=80, upload_to='user_profile', keep_meta=False, force_format='PNG'
    default='user_profile/user_placeholder.png')

class CardCategorie(models.Model):
    categorie = models.CharField(max_length=75, null=False, blank=False)

class CardAnime(models.Model):
    anime = models.CharField(max_length=200, null=False, blank=False)

class Card(models.Model):
    categorie = models.ForeignKey(CardCategorie, on_delete=models.CASCADE)
    anime = models.ForeignKey(CardAnime, on_delete=models.CASCADE)
    name = models.CharField(max_length=120, null=False, blank=False)
    description = models.TextField(max_length=500, null=False, blank=False)
    rarity = models.IntegerField()
    date_added = models.DateField()
